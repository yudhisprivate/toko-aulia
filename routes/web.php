<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\KasirController;

Route::get('/', [KasirController::class, 'index']);
Route::post('/kasir', [KasirController::class, 'store']);
Route::put('/kasir/{id}', [KasirController::class, 'update']);
Route::post('/kasir/transaksi', [KasirController::class, 'storeTransaction']);
Route::post('/kasir/transaksi', [KasirController::class, 'storeTransaction'])->name('transaksi.store');
