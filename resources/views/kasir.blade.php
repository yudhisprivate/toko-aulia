@extends('layouts.app')

@section('title', 'Kasir')

@section('content')
    <div class="flex">
        <div class="w-1/3 bg-white p-6 rounded-lg shadow-lg">
            <h2 class="text-2xl font-bold mb-4">Kasir</h2>

            <form method="POST" action="/kasir">
                @csrf

                <div class="mb-4">
                    <label class="block text-gray-700">Nama Kasir:</label>
                    <input class="w-full px-3 py-2 border rounded-md" type="text" value="Aulia" readonly>
                </div>

                <div class="mb-4">
                    <label class="block text-gray-700">Masukkan Uang:</label>
                    <select class="w-full px-3 py-2 border rounded-md" name="uang">
                        <option value="100">Rp. 100</option>
                        <option value="500">Rp. 500</option>
                        <option value="1000">Rp. 1000</option>
                        <option value="2000">Rp. 2000</option>
                        <option value="5000">Rp. 5000</option>
                        <option value="10000">Rp. 10000</option>
                        <option value="20000">Rp. 20000</option>
                        <option value="50000">Rp. 50000</option>
                        <option value="100000">Rp. 100000</option>
                    </select>
                </div>

                <div class="mb-4">
                    <label class="block text-gray-700">Total Uang yang Dimasukkan:</label>
                    <input class="w-full px-3 py-2 border rounded-md" type="number" name="total_dimiliki" value="1">
                </div>

                <button class="px-4 py-2 bg-blue-500 text-white rounded-md" type="submit">Simpan</button>
            </form>
        </div>

        <div class="w-1/3 ml-4 bg-white p-6 rounded-lg shadow-lg">
            <h2 class="text-2xl font-bold mb-4">Transaksi</h2>
            <form method="POST" action="{{ route('transaksi.store') }}">
                @csrf
                <div class="mb-4">
                    <label class="block text-gray-700">Nama Pembeli:</label>
                    <input class="w-full px-3 py-2 border rounded-md" type="text" name="nama_pelanggan" value="Yoga">
                </div>

                <div class="mb-4">
                    <label class="block text-gray-700">Harga Barang:</label>
                    <input class="w-full px-3 py-2 border rounded-md" type="number" name="harga_barang">
                </div>

                <div id="paymentContainer">
                    <div class="paymentGroup">
                        <div id="pembayaranInputs" class="mb-4">
                            <label class="block text-gray-700">Pembayaran:</label>
                            <input class="w-full px-3 py-2 border rounded-md" type="number" name="pembayaran[]">
                        </div>
                        <div class="mb-4">
                            <label class="block text-gray-700">Total Lembar:</label>
                            <input id="total_dimiliki" class="w-full px-3 py-2 border rounded-md" type="number"
                                name="total_dimiliki[]" value="1">
                        </div>
                    </div>
                </div>
                <button id="tambah" type="button">+</button>


                <button class="px-4 py-2 bg-blue-500 text-white rounded-md" type="submit">Simpan Transaksi</button>
            </form>

            @if (session('kembalian'))
                <div class="mt-4">
                    <h3 class="text-xl font-bold">Kembalian:</h3>
                    @foreach (session('kembalian') as $denomination => $count)
                        <p>Uang Rp. {{ $denomination }}: {{ $count }} lembar</p>
                    @endforeach
                </div>
            @endif
        </div>


        <div class="w-1/3 ml-4 bg-white p-6 rounded-lg shadow-lg">
            <h2 class="text-2xl font-bold mb-4">Monitor Keuangan</h2>

            @foreach ($kasir as $dataKasir)
                <p>Rp. {{ number_format($dataKasir->uang, 0, ',', '.') }} ada {{ $dataKasir->total_dimiliki }} lembar</p>
            @endforeach

            <h3 class="text-xl font-bold mt-4">Total Uang: Rp. {{ number_format($totalUang, 0, ',', '.') }}</h3>
        </div>
    </div>

    <script>
        document.getElementById('tambah').addEventListener('click', function() {
            var paymentContainer = document.getElementById('paymentContainer');
            var newGroup = document.createElement('div');
            newGroup.className = 'paymentGroup';
            newGroup.innerHTML = document.querySelector('.paymentGroup').innerHTML;
            paymentContainer.appendChild(newGroup);
        });
    </script>
@endsection
