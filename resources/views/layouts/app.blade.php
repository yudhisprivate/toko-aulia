<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
</head>
<body class="bg-red-500 flex items-center justify-center h-screen">
    <div class="bg-white p-6 rounded-lg shadow-lg">
        @yield('content')
    </div>
</body>
</html>
