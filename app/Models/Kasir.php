<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kasir extends Model
{
    protected $table = 'kasir';
    protected $fillable = ['nama', 'uang', 'total_dimiliki'];

    public function transaksi()
    {
        return $this->hasMany(Transaksi::class, 'id_kasir');
    }
}
