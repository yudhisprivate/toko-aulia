<?php

namespace App\Http\Controllers;

use App\Models\Kasir;
use App\Models\Pelanggan;
use App\Models\Transaksi;
use Illuminate\Http\Request;

class KasirController extends Controller
{
    public function index()
    {
        $kasir = Kasir::all(); // mengambil semua kasir
        $totalUang = $this->monitorMoney();
        return view('kasir', compact('kasir', 'totalUang'));
    }

    public function store(Request $request)
    {
        $kasir = new Kasir;

        $uang = $request->input('uang'); // misalnya, Rp. 5000
        $total_dimiliki = $request->input('total_dimiliki', 1); // jika tidak ada input, nilai default adalah 1

        // Menyetel data kasir
        $kasir->nama = 'Aulia';
        $kasir->uang = $uang;
        $kasir->total_dimiliki = $total_dimiliki;
        $kasir->save();

        return back()->with('success', 'Data kasir berhasil ditambahkan');
    }
    public function storeTransaction(Request $request)
    {
        // Ambil data dari permintaan
        $namaPelanggan = $request->input('nama_pelanggan', 'Yoga');
        $hargaBarang = $request->input('harga_barang'); // misalnya, Rp. 7500
        $totalDimiliki = $request->input('total_dimiliki'); // ini akan menghasilkan array
        $pembayaran = array_sum($request->input('pembayaran')); // misalnya, Rp. 10000 + Rp. 1000


        // Hitung kembalian
        $kembalian = $pembayaran - $hargaBarang; // misalnya, Rp. 2500

        // Cari atau buat pelanggan dengan nama yang diberikan
        $pelanggan = Pelanggan::firstOrCreate(['nama' => $namaPelanggan], ['uang' => $pembayaran, 'total_dimiliki' => 1]);

        // Dapatkan kasir yang namanya 'Aulia'
        $kasir = Kasir::where('nama', 'Aulia')->first();

        // Buat transaksi baru
        $transaksi = new Transaksi;
        $transaksi->id_pelanggan = $pelanggan->id;
        $transaksi->id_kasir = $kasir->id; // Gunakan ID kasir yang kita dapatkan sebelumnya
        $transaksi->total_transaksi = $hargaBarang;
        $transaksi->total_kembalian = $kembalian;
        $transaksi->save();

        // Tambahkan total uang yang dibayarkan oleh pelanggan ke uang yang dimiliki oleh Aulia
        $kasir->uang += $pembayaran;
        foreach ($totalDimiliki as $dimiliki) {
            $kasir->total_dimiliki += $dimiliki;
        }

        // Denominasi uang yang ada di kasir
        $denominations = [100000, 50000, 20000, 10000, 5000, 2000, 1000, 500, 100];
        $return = [];

        // Hitung berapa banyak dari setiap denominasi yang harus dikembalikan
        foreach ($denominations as $denomination) {
            $count = intdiv($kembalian, $denomination);
            if ($count > 0) {
                $kembalian %= $denomination;
                $return[$denomination] = $count;

                // Cek apakah kasir memiliki cukup uang
                if ($kasir->uang >= $count * $denomination) {
                    $kasir->uang -= $count * $denomination;
                    foreach ($totalDimiliki as $dimiliki) {
                        $kasir->total_dimiliki -= $dimiliki;
                    }
                } else {
                    // Jika tidak, batalkan transaksi dan kirim pesan kesalahan
                    return back()->with('error', 'Kasir tidak memiliki cukup uang untuk kembalian');
                }
            }
        }

        $kasir->save();

        // Kembalikan ke halaman sebelumnya dengan pesan sukses dan kembalian
        return back()->with([
            'success' => 'Transaksi berhasil disimpan',
            'kembalian' => $return
        ]);
    }
    public function monitorMoney()
    {
        // Ambil semua data kasir yang dimiliki oleh Aulia
        $kasir = Kasir::where('nama', 'Aulia')->get();

        // Hitung total uang yang dimiliki oleh Aulia
        $totalUang = $kasir->sum(function ($kas) {
            return $kas->uang * $kas->total_dimiliki;
        });

        // Mengembalikan total uang
        return $totalUang;
    }
}
