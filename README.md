# **How to running**

Berikut adalah langkah-langkah untuk menjalankan aplikasi ini:

1. **Clone repository**
    - Buka terminal Anda.
    - Jalankan perintah berikut untuk meng-clone repositori:
        ```
        git clone https://gitlab.com/yudhisprivate/toko-aulia.git
        ```
    - Pindah ke direktori proyek dengan perintah `cd toko-aulia`.

2. **Setup Environment**
    - Salin file .env.example menjadi .env menggunakan perintah berikut:
        ```
        cp .env.example .env
        ```
    - Kemudian, ubah pengaturan database dan lainnya sesuai dengan konfigurasi lokal Anda di file .env tersebut.

3. **Install dependencies**
    - Jalankan perintah berikut untuk menginstall dependencies yang diperlukan:
        ```
        composer install
        npm install
        ```

4. **Generate key**
    - Jalankan perintah berikut untuk generate key aplikasi Laravel:
        ```
        php artisan key:generate
        ```

5. **Migrate dan seed database**
    - Jalankan perintah berikut untuk melakukan migrasi dan seeding pada database:
        ```
        php artisan migrate --seed
        ```

6. **Running Laravel Sail (Docker)**
    - Untuk memulai Laravel Sail (Docker), gunakan perintah berikut:
        ```
        ./vendor/bin/sail up
        ```
    - Anda dapat mengakses aplikasi melalui `http://localhost`.

7. **Compile assets**
    - Jalankan perintah berikut untuk meng-compile assets:
        ```
        npm run dev
        ```
    - Atau jika Anda ingin assets di-compile secara otomatis setiap ada perubahan, Anda dapat menjalankan:
        ```
        npm run watch
        ```

8. **Testing**
    - Untuk menjalankan test, gunakan perintah berikut:
        ```
        ./vendor/bin/sail test
        ```

Setelah mengikuti langkah-langkah ini, Anda seharusnya sudah dapat menjalankan aplikasi dan mengaksesnya melalui browser Anda di `http://localhost`.
